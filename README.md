### tecnh_test

Este repositorio contiene tres carpetas, distribuidas de la siguiente manera:

1. Punto_1: Contiene el notebook del ejercicio del precio de la vivienda, este notebook tiene las respectivas explicaciones a las preguntas.
2. Punto_2: Contiene el notebook del análisis de sentimientos de las reviews de Amazon, este notebook contiene las respuestas a las preguntas.
3. Punto_3: Contiene el notebook del ejercicio de optimización, está diseñado con funciones de ejecución y de prueba.

